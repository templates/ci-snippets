#!/usr/bin/env bash

#!/usr/bin/env bash

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

set -e

#######

cd $CWD/..

LATEST=$(git log --format="%H" -n 1)

echo "CI snippets latest version is: $LATEST"

for template in $(ls .. )
do
	if [[ "$template" != "$(basename $(pwd))" ]] && [[ $template != *".code-workspace" ]]
	then
		echo "$template ..."
		cd $CWD/../../$template
		sed -i.bak "s/  CI_REF:.*/  CI_REF: \&ref $LATEST/g" .gitlab-ci.yml
		rm .gitlab-ci.yml.bak
		git add .gitlab-ci.yml
		git commit -m "Bump CI to the latest version."
	fi
done

echo "Done."
