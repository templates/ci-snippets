#!/usr/bin/env bash

set -e

JUNIT_FILE="./tidy-junit.xml"
PASS=()
FAIL=()

BASE_INTERNAL="https://$NAME.review.dbogatov.org"
if [ ! -z "$BASE" ]; then
	BASE_INTERNAL=$BASE
fi

for url in $TIDY_URLS
do
	curl -Ls $BASE_INTERNAL/$TIDY_PREFIX/$url$TIDY_SUFFIX | tidy $TIDY_CONFIG -e &> tidy.out || true
	cat tidy.out

	if cat tidy.out | grep -q '\- Warning:'
	then
		echo "FAILED for $TIDY_PREFIX/$url$TIDY_SUFFIX"
		FAIL+=( $TIDY_PREFIX/$url$TIDY_SUFFIX )
	else
		echo "Passed for $TIDY_PREFIX/$url$TIDY_SUFFIX"
		PASS+=( $TIDY_PREFIX/$url$TIDY_SUFFIX )
	fi
done

echo "<?xml version='1.0'?>" > "${JUNIT_FILE}"
echo "<testsuites>" >> "${JUNIT_FILE}"
echo "<testsuite name='tidy' tests='$(( ${#PASS[@]} + ${#FAIL[@]} ))' failures='$((${#FAIL[@]}))' skipped='0' time='0.0000'>" >> "${JUNIT_FILE}"

for test in "${PASS[@]}"
do
	echo "<testcase name='${test}' classname='tidy' time='0.0000'/>" >> "${JUNIT_FILE}"
done
for test in "${FAIL[@]}"
do
	echo "<testcase name='${test}' classname='tidy' time='0.0000'>" >> "${JUNIT_FILE}"
	echo "<failure message='malformed HTML'/>" >> "${JUNIT_FILE}"
	echo "</testcase>" >> "${JUNIT_FILE}"
done

echo "</testsuite>" >> "${JUNIT_FILE}"
echo "</testsuites>" >> "${JUNIT_FILE}"

if [ ${#FAIL[@]} -eq 0 ]; then
	echo "Checks passed!"
else
	echo "There are some failures..."
	exit 1
fi
