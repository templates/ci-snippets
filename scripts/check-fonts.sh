#!/usr/bin/env bash

set -e

if [ -z "$LATEX_DIST" ]
then
	LATEX_DIST="dist";
fi

for pdf in ./$LATEX_DIST/*.pdf
do
	echo "Checking $pdf..."

	list=$(pdffonts $pdf)
	i=0

	while read -r line; do
		if [ $i -ge 2 ];
		then
			# fifth from the end
			res=$(echo $line | rev | cut -d" " -f 5 | rev)
			[ $res == "yes" ];
		fi
		i=$((i+1))
	done <<< "$list"
done

echo "Checks passed!"
