#!/usr/bin/env bash

curl -k -s https://$TOKEN@token.dbogatov.org/gcloud-key > key.json
gcloud auth activate-service-account --key-file=key.json
export PROJECT=$(gcloud projects list --format="value(projectId)")
gcloud config set project $PROJECT
export ZONE=$(gcloud container clusters list --format="value(zone)")
export CLUSTER=$(gcloud container clusters list --format="value(name)")
gcloud container clusters get-credentials $CLUSTER --zone $ZONE --project $PROJECT
