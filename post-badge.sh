#!/usr/bin/env bash

set -e

NAME=$1
LABEL=$2
VALUE=$3
COLOR=$4
LINK=$5

if [ "$#" -ne 5 ]
then
	echo "Expecting 5 arguments"
	exit 1
fi

if [ -n "$PAT" ]
then
	PREV_ID=$(curl \
		-Ls \
		--header "PRIVATE-TOKEN: $PAT" \
		$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/badges \
	| jq 'map(select( .name == '\"$NAME\"' )) | if length > 0 then .[0] | .id else -1 end')

	if [ "$PREV_ID" -ne -1 ]
	then
		echo "Found badge with this name. Will delete it first."
		curl \
			-Ls \
			--request DELETE \
			--header "PRIVATE-TOKEN: $PAT" \
			$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/badges/$PREV_ID
	fi

	curl \
		--request POST \
		-Ls \
		--header "PRIVATE-TOKEN: $PAT" \
		--data "link_url=$LINK&image_url=https://img.shields.io/badge/$LABEL-$VALUE-$COLOR&name=$NAME" \
		$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/badges > /dev/null

	echo "Badge created!"
else
	echo "PAT not set. Will NOT post the badge."
fi
