#!/usr/bin/env bash

shopt -s globstar

# Ensure that the CWD is set to script's location
cd "${0%/*}"
CWD=$(pwd)

#######

DIST=$1

declare -i maxpages=0

for pdf in $(ls $DIST)
do
	pages=$(pdfinfo $DIST/$pdf | grep Pages | awk '{print $2}')
	if [ "$pages" -gt "$maxpages" ]
	then
		maxpages=$pages
	fi
done

echo $maxpages
